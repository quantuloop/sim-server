from quantuloop.sim_server import register, configure_server, AVAILABLE_QUANTUM_EXECUTION, start_server


for sim in AVAILABLE_QUANTUM_EXECUTION:
    register(sim, *AVAILABLE_QUANTUM_EXECUTION[sim])

configure_server(
    secret='321',
    file_log='/tmp/quloop_server_log.csv',
    stdout_log=True,
    timezone=-3
)

start_server()
