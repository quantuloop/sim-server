from __future__ import annotations
# Copyright 2023 Quantuloop
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import socket
from ctypes import c_uint8
from datetime import datetime, timezone, timedelta
from io import BytesIO
import json
from flask import Flask, request, send_file, make_response, jsonify
from quantuloop import quest, sparse, dense
from ket.clib import kbw
from ket.clib import libket
from os import environ
import secrets
from hashlib import sha256
from waitress import serve
from flask_cloudflared import _run_cloudflared
import jwt


class BaseQuantumExecution:
    def __init__(self,
                 run_serialized,
                 result_get,
                 result_delete):

        self.run_serialized = run_serialized
        self.result_get = result_get
        self.result_delete = result_delete

    def __call__(self, quantum_code: bytearray, quantum_metrics: bytearray) -> tuple[bytearray, int]:

        quantum_code_size = len(quantum_code)
        metrics_size = len(quantum_metrics)

        result = self.run_serialized(
            (c_uint8*quantum_code_size)(*quantum_code),
            quantum_code_size,
            (c_uint8*metrics_size)(*quantum_metrics),
            metrics_size,
            libket.JSON
        )

        data, size = self.result_get(result)
        exec_time = json.loads(bytearray(data[:size.value]))['exec_time']

        try:
            return bytearray(data[:size.value]), exec_time
        finally:
            self.result_delete(result)


QUANTUM_EXECUTION_REGISTER = {}
DEFAULT_QUANTUM_EXECUTION = None

AVAILABLE_QUANTUM_EXECUTION = {
    'KBW Dense': (lambda *args: kbw.API['kbw_run_serialized'](*args, kbw.DENSE),
                  kbw.API['kbw_result_get'],
                  kbw.API['kbw_result_delete']),
    'KBW Sparse': (lambda *args: kbw.API['kbw_run_serialized'](*args, kbw.SPARSE),
                   kbw.API['kbw_result_get'],
                   kbw.API['kbw_result_delete']),
    'Quantuloop Dense': (dense.API['quloop_dense_run_serialized'],
                         dense.API['kbw_result_get'],
                         dense.API['kbw_result_delete']),
    'Quantuloop Sparse': (sparse.API['quloop_sparse_run_serialized'],
                          sparse.API['kbw_result_get'],
                          sparse.API['kbw_result_delete'], True),
    'Quantuloop QuEST': (quest.API['quloop_quest_run_serialized'],
                         quest.API['kbw_result_get'],
                         quest.API['kbw_result_delete']),
}


def register(name: str, run_serialized, result_get, result_delete, default: bool = False):
    global QUANTUM_EXECUTION_REGISTER
    global DEFAULT_QUANTUM_EXECUTION

    QUANTUM_EXECUTION_REGISTER[name.upper()] = BaseQuantumExecution(
        run_serialized, result_get, result_delete)

    if default:
        DEFAULT_QUANTUM_EXECUTION = name.upper()


def get_execution(name: str):
    return QUANTUM_EXECUTION_REGISTER[name.upper()]


SERVER_CONFIGURATION = dict(
    secret=sha256(secrets.token_bytes(32)).hexdigest(),
    file_log=None,
    stdout_log=False,
    timezone=0,
)


def configure_server(*,
                     secret: str | None = None,
                     file_log: str | None = None,
                     stdout_log: bool | None = None,
                     timezone: int | None = None,
                     token: str | None = None,
                     token_file: str | None = None):
    global SERVER_CONFIGURATION

    if secret is not None:
        SERVER_CONFIGURATION["secret"] = sha256(
            secret.encode('utf-8')).hexdigest()

    if stdout_log is not None:
        SERVER_CONFIGURATION["stdout_log"] = stdout_log

    SERVER_CONFIGURATION["file_log"] = file_log
    if timezone is not None:
        SERVER_CONFIGURATION["timezone"] = timezone

    if token_file is not None:
        with open(token_file, 'r') as file:
            token = file.read()

    if token is not None:
        environ['QULOOP_TOKEN'] = token


app = Flask(__name__)


@app.route("/")
def home():
    message = "<h1>Welcome to Quantuloop Quantum Simulator Suite for HPC!</h1>"
    message += "<h2>Registered Quantum Simulators:</h2>"
    message += "<ul>"
    for name in QUANTUM_EXECUTION_REGISTER:
        message += "<li>" + name
        if name == DEFAULT_QUANTUM_EXECUTION:
            message += "*"
        message + "</li>"
    message += "</ul>"

    message += "<p>*default quantum simulator.</p>"

    return message


@app.route("/run")
def execute_quantum_code():
    token = request.files['token'].read().decode()
    try:
        jwt.decode(token,
                   key=SERVER_CONFIGURATION['secret'],
                   algorithms='HS256')
    except:
        return make_response(jsonify(dict(status='fail', message='fail to authenticate'))), 500

    simulator = request.args.get(
        "simulator", default=DEFAULT_QUANTUM_EXECUTION, type=str)
    if simulator not in QUANTUM_EXECUTION_REGISTER:
        simulator = DEFAULT_QUANTUM_EXECUTION

    precision = request.args.get("precision", default=1, type=int)
    gpu_count = request.args.get("ngpu", default=0, type=int)

    environ['QULOOP_FP'] = str(precision)
    environ['QULOOP_GPU'] = str(gpu_count)

    quantum_metrics_raw = request.files['quantum_metrics'].read()
    quantum_metrics = json.loads(quantum_metrics_raw.decode())

    result, exec_time = QUANTUM_EXECUTION_REGISTER[simulator](
        quantum_code=request.files['quantum_code'].read(),
        quantum_metrics=quantum_metrics_raw
    )

    date = datetime.now(timezone(
        timedelta(hours=SERVER_CONFIGURATION["timezone"])))
    date = date.strftime('%Y.%m.%d %H:%M:%S')

    msg = f'"{date}","{simulator}",{precision},{gpu_count},{quantum_metrics["qubit_simultaneous"]},{quantum_metrics["block_count"]},{exec_time}\n'

    if SERVER_CONFIGURATION['file_log'] is not None:
        with open(SERVER_CONFIGURATION["file_log"], 'a') as log:
            log.write(msg)

    if SERVER_CONFIGURATION["stdout_log"]:
        print(msg, end='')

    return send_file(
        BytesIO(result),
        mimetype='application/octet-stream',
        download_name='result.json',
        as_attachment=True,
    )


@app.route("/simulators", methods=['GET'])
def get_backends():
    return jsonify(list(QUANTUM_EXECUTION_REGISTER))


def find_port(port=4242):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        if s.connect_ex(("localhost", port)) == 0:
            return find_port(port=port + 1)
        else:
            return port


def start_server(bind: str = '127.0.0.1', port: int | None = None, threads: int = 2, tunnel: bool | None = False, server_url: str | None = None):
    if port is None:
        port = find_port()

    if tunnel:
        url = _run_cloudflared(port)
    elif server_url:
        url = f'{server_url}:{port}'
    else:
        url = f'http://{bind}:{port}'

    payload = dict(url=url)

    token = jwt.encode(payload=payload,
                       key=SERVER_CONFIGURATION['secret'],
                       algorithm='HS256')

    print(f'''
Your Server token is:
    {token}

You can install Quantuloop Quantum Simulator Suite Client with the command:
    pip install git+https://gitlab.com/quantuloop/client.git

In your Ket application, use the following code to access the simulation server:

    from quantuloop import client
    client.set_server('{token}')
 
The default quantum simulator is:
    {DEFAULT_QUANTUM_EXECUTION}

You can change the quantum simulator with: 
    client.set_simulator('SIMULATOR NAME')

You can get a list of the available simulator with:
    client.available_simulators()

To stop the server, press Ctrl+C or stop the cell.
''')

    serve(app, host=bind, port=port, threads=threads)
